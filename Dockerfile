FROM quay.io/jupyter/datascience-notebook:hub-4.1.5

ARG MOSEKLIC

USER root

COPY start-singleuser.sh /usr/local/bin/
COPY mosek.lic /usr/local/share/mosek.lic

RUN apt-get update && apt-get upgrade -y && \
  apt-get install -y \
    acl \
    cmake \
    gpg \
    less \
    libgl1-mesa-glx \
    libglu1-mesa \
    libgtk-3-0 \
    libxft2 \
    libxdamage-dev \
    lmodern \
    man \
    texlive-xetex \
    texlive-lang-german \
    texlive-science \
    vim \
    zip

RUN mamba env create -f /builds/eduit/images/notebooks/24fs/jh-notebook-coco/requirements.yml && \
  source activate coco &&\
  python -m ipykernel install --name=coco --display-name='Computational Control'

USER jovyan
